import netP5.*; 
import oscP5.*;

final color LIBERTY_COLOR = color(0, 85, 164);
final color EQUITY_COLOR = color (255, 255, 255);
final color BROTHERHOOD_COLOR = color(239, 65, 53);

OscP5 oscP5;
NetAddress table;

static final String tableIP = "127.0.0.1";
static final int tablePort = 3333;

boolean liberty, equity, brotherhood;

void setup() {
  size(300, 200);
  oscP5 = new OscP5(this, 12000); // 3333 is the  port that reactiVIsion uses
  table = new NetAddress(tableIP, tablePort);
  oscP5.plug(this, "handlePuzzle", "/puzzle", "s");

  noStroke();
}

void draw() {
  background(0);

  if ( liberty  == true) { 
    fill(LIBERTY_COLOR); 
    rect(0, 0, 100, height);
  }

  if (equity == true) { 
    fill(EQUITY_COLOR); 
    rect(100, 0, 100, height);
  }

  if ( brotherhood == true) { 
    fill(BROTHERHOOD_COLOR); 
    rect(200, 0, 100, height);
  }
}

void handlePuzzle(String puzzle) {
  if (puzzle.equals("liberty")) {
    liberty = true;
    OscMessage m = new OscMessage("/confirm");
    m.add(puzzle);
    oscP5.send(m, table);
  } else if (puzzle.equals("equity")) {
    equity = true;
    OscMessage m = new OscMessage("/confirm");
    m.add(puzzle);
    oscP5.send(m, table);
  } else if (puzzle.equals("brotherhood")) {
    brotherhood = true;
    OscMessage m = new OscMessage("/confirm");
    m.add(puzzle);
    oscP5.send(m, table);
  }
}

void oscEvent(OscMessage m) {
}
