EESchema Schematic File Version 4
LIBS:shield-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Module:Arduino_UNO_R3 A1
U 1 1 5BF25565
P 5000 1900
F 0 "A1" H 5000 3078 50  0000 C CNN
F 1 "Arduino_UNO_R3" H 5000 2987 50  0000 C CNN
F 2 "Module:Arduino_UNO_R3" H 5150 850 50  0001 L CNN
F 3 "https://www.arduino.cc/en/Main/arduinoBoardUno" H 4800 2950 50  0001 C CNN
	1    5000 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5BF255DC
P 4150 2300
F 0 "R2" V 3943 2300 50  0000 C CNN
F 1 "220" V 4034 2300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 4080 2300 50  0001 C CNN
F 3 "~" H 4150 2300 50  0001 C CNN
	1    4150 2300
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5BF2570B
P 4150 2000
F 0 "R1" V 3943 2000 50  0000 C CNN
F 1 "220" V 4034 2000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 4080 2000 50  0001 C CNN
F 3 "~" H 4150 2000 50  0001 C CNN
	1    4150 2000
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5BF2575D
P 4150 2600
F 0 "R3" V 3943 2600 50  0000 C CNN
F 1 "220" V 4034 2600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 4080 2600 50  0001 C CNN
F 3 "~" H 4150 2600 50  0001 C CNN
	1    4150 2600
	0    1    1    0   
$EndComp
$Comp
L Device:LED D2
U 1 1 5BF25988
P 3800 2000
F 0 "D2" H 3791 2216 50  0000 C CNN
F 1 "LED" H 3791 2125 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 3800 2000 50  0001 C CNN
F 3 "~" H 3800 2000 50  0001 C CNN
	1    3800 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 2000 3950 2000
$Comp
L Device:LED D1
U 1 1 5BF25C4D
P 3500 2300
F 0 "D1" H 3491 2516 50  0000 C CNN
F 1 "LED" H 3491 2425 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 3500 2300 50  0001 C CNN
F 3 "~" H 3500 2300 50  0001 C CNN
	1    3500 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 2300 4000 2300
$Comp
L Device:LED D3
U 1 1 5BF26028
P 3800 2600
F 0 "D3" H 3791 2816 50  0000 C CNN
F 1 "LED" H 3791 2725 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 3800 2600 50  0001 C CNN
F 3 "~" H 3800 2600 50  0001 C CNN
	1    3800 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 2600 4000 2600
$Comp
L power:GND #PWR0101
U 1 1 5BF266B0
P 3250 3200
F 0 "#PWR0101" H 3250 2950 50  0001 C CNN
F 1 "GND" H 3255 3027 50  0000 C CNN
F 2 "" H 3250 3200 50  0001 C CNN
F 3 "" H 3250 3200 50  0001 C CNN
	1    3250 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 2600 3250 2600
Wire Wire Line
	3350 2300 3250 2300
Wire Wire Line
	3650 2000 3250 2000
Connection ~ 3250 2600
Wire Wire Line
	3250 2000 3250 2300
Wire Wire Line
	3250 2300 3250 2600
Connection ~ 3250 2300
Wire Wire Line
	5100 3000 5000 3000
Wire Wire Line
	3250 2600 3250 3000
Connection ~ 3250 3000
Wire Wire Line
	3250 3000 3250 3200
Connection ~ 4900 3000
Wire Wire Line
	4900 3000 3250 3000
Connection ~ 5000 3000
Wire Wire Line
	5000 3000 4900 3000
NoConn ~ 5500 2700
NoConn ~ 5500 2600
NoConn ~ 5500 2400
NoConn ~ 5500 2300
NoConn ~ 5500 2200
NoConn ~ 5500 2100
NoConn ~ 5500 2000
NoConn ~ 5500 1900
NoConn ~ 5500 1700
NoConn ~ 5500 1500
NoConn ~ 5500 1300
NoConn ~ 4500 2600
NoConn ~ 4500 2100
NoConn ~ 4500 2000
NoConn ~ 4500 1700
NoConn ~ 4500 1600
NoConn ~ 4500 1500
NoConn ~ 4500 1400
NoConn ~ 4500 1300
NoConn ~ 4900 900 
NoConn ~ 5100 900 
$Comp
L Switch:SW_Push SW1
U 1 1 5BF33449
P 3700 1200
F 0 "SW1" H 3700 1485 50  0000 C CNN
F 1 "SW_Push" H 3700 1394 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 3700 1400 50  0001 C CNN
F 3 "" H 3700 1400 50  0001 C CNN
	1    3700 1200
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 5BF33A76
P 3700 1600
F 0 "SW2" H 3700 1885 50  0000 C CNN
F 1 "SW_Push" H 3700 1794 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 3700 1800 50  0001 C CNN
F 3 "" H 3700 1800 50  0001 C CNN
	1    3700 1600
	1    0    0    -1  
$EndComp
NoConn ~ 5200 900 
NoConn ~ 4500 2500
Wire Wire Line
	4500 2200 4400 2200
Wire Wire Line
	4400 2200 4400 2000
Wire Wire Line
	4400 2000 4300 2000
Wire Wire Line
	4500 2300 4300 2300
Wire Wire Line
	4500 2400 4400 2400
Wire Wire Line
	4400 2400 4400 2600
Wire Wire Line
	4400 2600 4300 2600
Wire Wire Line
	4500 1900 4300 1900
Wire Wire Line
	4300 1900 4300 1600
Wire Wire Line
	4300 1600 3900 1600
Wire Wire Line
	3900 1200 4400 1200
Wire Wire Line
	4400 1200 4400 1800
Wire Wire Line
	4400 1800 4500 1800
Wire Wire Line
	3500 1600 3250 1600
Wire Wire Line
	3250 1600 3250 2000
Connection ~ 3250 2000
Wire Wire Line
	3250 1200 3250 1600
Wire Wire Line
	3250 1200 3500 1200
Connection ~ 3250 1600
Wire Wire Line
	3250 1600 3250 2000
$EndSCHEMATC
