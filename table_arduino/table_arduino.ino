#include <SoftwareSerial.h>
#include <SerialCommand.h>

//https://github.com/scogswell/ArduinoSerialCommand
SerialCommand s;

// Config
float pulseVel;

const int ledL = 12;
const int ledE = 10;
const int ledB = 11;

// States
const int UNSOLVED = 0;
const int SENDING = 1;
const int RECEIVED = 2;

int liberty, equity, brotherhood;
boolean directionL, directionE, directionB;
float lightL, lightE, lightB;

void setup() {
  Serial.begin(9600);     // opens serial port, sets data rate to 9600 bps

  pinMode(ledL, OUTPUT);
  pinMode(ledE, OUTPUT);
  pinMode(ledB, OUTPUT);

  lightL = lightE = lightB = 0;
  directionL = directionE = directionB = true;
  pulseVel = 0.05;

  reset();

  s.addCommand("set", set);
  s.addCommand("reset", reset);

  s.addDefaultHandler(unrecognized);
  Serial.println("Ready");
}

/*
   Handler for serial.
   Send "set [leb] [usr]" via serial with a carriage return
   to set led to specific state;
*/

void reset() {
  Serial.println("Restarting");
  liberty = UNSOLVED;
  equity = UNSOLVED;
  brotherhood = UNSOLVED;
}

void set() {
  char *arg;
  arg = s.next();

  int *led;

  if (arg != NULL) {
    if (arg[0] == 'l') {
      led = &liberty;
    } else if (arg[0] == 'e') {
      led = &equity;
    } else if (arg[0] == 'b') {
      led = &brotherhood;
    } else {
      Serial.println("Expected [leb] as the first parameter");
    }
  }

  String l;
  if (*led == liberty) {
    l = "liberty";
  } else if (*led == equity) {
    l = "equity";
  } else if ( *led == brotherhood) {
    l = "brotherhood";
  } else {
    l = "undefined";
  }

  arg = s.next();
  if (arg != NULL) {
    if (arg[0] == 'u') {
      *led = UNSOLVED;
      Serial.println("Changed " + l + " to UNSOLVED");
    } else if (arg[0] == 's') {
      *led = SENDING;
      Serial.println("Changed " + l + " to SENDING");
    } else if (arg[0] == 'r') {
      *led = RECEIVED;
      Serial.println("Changed " + l + " to RECEIVED");
    } else {
      Serial.println("Expected [usr] as the second parameter");
    }


  } else {
    Serial.println("Expected two parameters sperated by a space: [leb] [usr]");
  }
}

void unrecognized()
{
  Serial.println("What?");
}

void loop() {
  s.readSerial();     // We don't do much, just process serial commands

  handleLEDs();

  analogWrite(ledL, lightL);
  analogWrite(ledE, lightE);
  analogWrite(ledB, lightB);
}

void handleLEDs() {
  switch (liberty) {
    case UNSOLVED: {
        powerOff(lightL, directionL);
        break;
      }
    case SENDING: {
        pulse(lightL, directionL);
        break;
      }
    case RECEIVED: {
        powerOn(lightL, directionL);
        break;
      }
  }

  switch (equity) {
    case UNSOLVED: {
        powerOff(lightE, directionE);
        break;
      }
    case SENDING: {
        pulse(lightE, directionE);
        break;
      }
    case RECEIVED: {
        powerOn(lightE, directionE);
        break;
      }
  }

  switch (brotherhood) {
    case UNSOLVED: {
        powerOff(lightB, directionB);
        break;
      }
    case SENDING: {
        pulse(lightB , directionB);
        break;
      }
    case RECEIVED: {
        powerOn(lightB, directionB);
        break;
      }
  }
}

void pulse(float &light, boolean &direction) {
  if (direction) {
    powerOn(light, direction);
  } else {
    powerOff(light, direction);
  }
}

void powerOn(float &light, boolean &direction) {
  if (light <= 255) {
    light += pulseVel * 0.25;
  }
  if (light >= 255) {
    direction = false;
  }
}

void powerOff(float &light, boolean &direction) {
  if (light >= 0) {
    direction = false;
    light -= pulseVel * 0.25;
  }

  else if (light <= 0) {
    direction = true;
  }
}
