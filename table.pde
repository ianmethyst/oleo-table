import ddf.minim.*; //<>//
import processing.serial.*;
import netP5.*; 
import oscP5.*;

OscP5 oscP5;
NetAddress sar;

static final String sarIP = "127.0.0.1";
static final int sarPort = 12000;

Serial serialPort;
String serialString = null;
boolean serialReady; 
static final int LF = 10;    // Linefeed in ASCII

Minim minim;
AudioSample sendSound;

// Variables and constants for visual debugging.
static final int ELLIPSE_SIZE = 50;
static final String SOLVE_TEXT = "Sin resolver";
static final String WAIT_TEXT = "Esperando";
static final String SEND_TEXT = "Enviando";
static final String RECEIVED_TEXT = "Recibido";
final color LIBERTY_COLOR = color(0, 85, 164);
final color EQUITY_COLOR = color (255, 255, 255);
final color BROTHERHOOD_COLOR = color(239, 65, 53);
final color GRAY_COLOR = color(175, 175, 175);

float center;

// Config variables. Change this via OSC to fine tune.
PVector cameraOrigin;
PVector cameraDimensions;
int minimumMarkers;
int aliveTime;
float libertyDistance;
float brotherhoodDistance;
float equityAngle;
float timeBeforeSend;

// State constants
static final int UNSOLVED = 0;
static final int WAITING = 1;
static final int SENDING = 2;
static final int RECEIVED = 3;

// If any puzzle is solved, these are set to true. 
// Used in conjuction with timeBeforeSend to check when the message should be sent.
int liberty;
int equity;
int brotherhood;

// Variables for communication.
int startTime;
boolean waiting;
int nextTryTime;

// Objects 
ArrayList<Object> objects = new ArrayList<Object>();

void setup() {
  // Change to match camera
  frame.setTitle("Hello");
  size(640, 580);

  println(sketchPath(""));

  minim = new Minim(this);
  sendSound = minim.loadSample("sound/send.wav", 512);

  // Asthethics 
  textSize(24);
  textAlign(CENTER, TOP);
  ellipseMode(CENTER);
  colorMode(HSB, 360, 100, 100);
  fill(255, 0, 0);
  //noStroke();
  stroke(255);

  // Config
  cameraOrigin = new PVector(0, 100);
  cameraDimensions = new PVector(640, 480);
  minimumMarkers = 5;
  libertyDistance = 190;
  brotherhoodDistance = 225;
  equityAngle = 5;
  aliveTime = 3000;
  timeBeforeSend = 2000;

  // Initialization of variables
  center = width * 0.5;
  hardResetPrincilples();

  startTime = -1;
  nextTryTime = -1;
  waiting = false;

  oscP5 = new OscP5(this, 3333); // 3333 is the  port that reactiVIsion uses
  sar = new NetAddress(sarIP, sarPort);

  oscP5.plug(this, "TUIOSet", "/tuio/2Dobj", "siiffffffff");
  oscP5.plug(this, "handleConfirm", "/confirm", "s");
  oscP5.plug(this, "handleReset", "/reset", "i");

  String port = findPort();
  if (port != null) {
    serialPort = new Serial(this, Serial.list()[0], 9600);
  }
}

void handleReset(int i) {
  hardResetPrincilples();
}

void handleConfirm(String puzzle) {
  sendSound.trigger();
  if (puzzle.equals("liberty")) {
    liberty = RECEIVED;
    writeSerial("set l r");
  } else if (puzzle.equals("equity")) {
    equity = RECEIVED;
    writeSerial("set e r");
  } else if (puzzle.equals("brotherhood")) {
    brotherhood = RECEIVED;
    writeSerial("set b r");
  }
}

void draw() {
  background(0);

  readSerial();

  int currentTime = millis();

  renderObjectsTransformed();
  checkState(currentTime);
  sendMessage(currentTime);
  drawState();
  softResetPrinciples();
}

// Functions to be called in draw (for clarity)

void sendMessage(int currentTime) {
  if (currentTime > nextTryTime) {
    nextTryTime = currentTime + 1000;
    if (liberty == SENDING) {
      println("Trying to send message: liberty");
      OscMessage m = new OscMessage("/puzzle");
      m.add("liberty");
      oscP5.send(m, sar);
    }
    if (equity == SENDING) {
      println("Trying to send message: equity");
      OscMessage m = new OscMessage("/puzzle");
      m.add("equity");
      oscP5.send(m, sar);
    } 
    if (brotherhood == SENDING) {
      println("Trying to send message: brotherhood");
      OscMessage m = new OscMessage("/puzzle");
      m.add("brotherhood");
      oscP5.send(m, sar);
    }
  }
}

void checkState(int currentTime) {
  if (liberty != WAITING && equity != WAITING && brotherhood != WAITING) {
    waiting = false;
    startTime = -1;
  } else {
    if (!waiting) {
      waiting = true;
      startTime = currentTime;
    } else {
      if (currentTime > startTime + timeBeforeSend) {
        if (liberty == WAITING) {
          liberty = SENDING;
          writeSerial("set l s");
        } else if (equity == WAITING) {
          equity = SENDING;
          writeSerial("set e s");
        } else if ( brotherhood == WAITING) {
          brotherhood = SENDING;
          writeSerial("set b s");
        }
      }
    }
  }
}

void drawState() {
  pushStyle();
  if (objects.size() >= minimumMarkers) {
    textAlign(RIGHT, TOP);
    if (liberty == WAITING) {
      fill(LIBERTY_COLOR);
    } else { 
      fill(GRAY_COLOR);
    }
    text("LIBERTAD ", center, 0);

    if (equity == WAITING) {
      fill(EQUITY_COLOR);
    } else { 
      fill(GRAY_COLOR);
    }
    text("\nIGUALDAD ", center, 0);

    if (brotherhood == WAITING) {
      fill(BROTHERHOOD_COLOR);
    } else { 
      fill(GRAY_COLOR);
    }
    text("\n\nFRATERNIDAD ", center, 0);

    textAlign(LEFT, TOP);   
    fill(GRAY_COLOR);

    pushStyle();
    switch(liberty) {
    case UNSOLVED:
      text(SOLVE_TEXT, center, 0);
      break;
    case WAITING:
      text(WAIT_TEXT, center, 0);
      break;
    case SENDING:
      fill(LIBERTY_COLOR);
      text(SEND_TEXT, center, 0);
      break;
    case RECEIVED:
      fill(LIBERTY_COLOR);
      text(RECEIVED_TEXT, center, 0);
      break;
    }
    popStyle();

    pushStyle();
    switch(equity) {
    case UNSOLVED:
      text("\n" + SOLVE_TEXT, center, 0);
      break;
    case WAITING:
      text("\n" + WAIT_TEXT, center, 0);
      break;
    case SENDING:
      fill(EQUITY_COLOR);
      text("\n" + SEND_TEXT, center, 0);
      break;
    case RECEIVED:
      fill(EQUITY_COLOR);
      text("\n" + RECEIVED_TEXT, center, 0);
      break;
    }
    popStyle();

    pushStyle();
    switch(brotherhood) {
    case UNSOLVED:
      text("\n\n" + SOLVE_TEXT, center, 0);
      break;
    case WAITING:
      text("\n\n" + WAIT_TEXT, center, 0);
      break;
    case SENDING:
      fill(BROTHERHOOD_COLOR);
      text("\n\n" + SEND_TEXT, center, 0);
      break;
    case RECEIVED:
      fill(BROTHERHOOD_COLOR);
      text("\n\n" + RECEIVED_TEXT, center, 0);
      break;
    }
    popStyle();
  } else {
    fill(GRAY_COLOR);
    text("\nFaltan " + (minimumMarkers - objects.size()) + " marcadores.", width * 0.5, 0);
  }
  popStyle();
}


void renderObjectsTransformed() {
  pushStyle();
  pushMatrix();
  clip(cameraOrigin.x, cameraOrigin.y, cameraDimensions.x, cameraDimensions.y);
  translate(cameraOrigin.x, cameraOrigin.y);
  stroke(0, 0, 100);
  noFill();
  rect(0, 0, cameraDimensions.x - 1, cameraDimensions.y - 1);
  testPrinciples();
  popMatrix();
  noClip();
  popStyle();
}

void testPrinciples() {
  // Variables used if the puzzles were solved in every draw cycle.
  int libertyCounter = 0;
  int brotherhoodCounter = 0;

  for (int i = 0; i < objects.size(); i++) {
    Object obj = objects.get(i);

    // Variables used to compare object i with object j (inner for loop)
    int b = 0;
    int l = 0;
    int e = 0;
    float initialAngle = 0;
    boolean initialAngleSet = false; 

    // If there is more than one object, comapre them
    if (objects.size() > 1) {
      for (int j = 0; j < objects.size(); j++) {
        if (j != i) {
          Object another = objects.get(j);

          PVector position = obj.getPosition();
          PVector targetPos = another.getPosition();

          stroke(GRAY_COLOR);
          if (equity != WAITING) {
            if (obj.getPosition().dist(targetPos) < brotherhoodDistance) {
              if (brotherhood == UNSOLVED || brotherhood == WAITING) {
                stroke(BROTHERHOOD_COLOR);
                b++;
              }
            } else if (obj.getPosition().dist(targetPos) > libertyDistance) {
              if (liberty == UNSOLVED || liberty == WAITING) {
                stroke(LIBERTY_COLOR);
                l++;
              }
            } 

            if (equity == UNSOLVED) {
              // Check for equity if there's more than two objects
              if (objects.size() > 2) {
                float a = degrees(atan2(targetPos.y - position.y, targetPos.x - position.x));

                if (!initialAngleSet) {
                  initialAngle = a;
                  initialAngleSet = true;
                } else {
                  if (a < initialAngle + equityAngle && a > initialAngle - equityAngle) {
                    e++;
                  }
                }
                if (objects.size() >= minimumMarkers && e == objects.size() - 2) {
                  equity = WAITING;
                }
              }
            }
          } else if (equity == WAITING) {
            stroke(EQUITY_COLOR);
          }

          obj.drawHelperLine(another.getPosition());
        }
      }

      if (b == objects.size() - 1) {
        brotherhoodCounter++;
      } else if (l == objects.size() - 1) {
        libertyCounter++;
      }
    }

    // Remove obj if time exceeded
    if (obj.getAging() && obj.getTime() - millis() < 0) {
      println("Object removed. Fiducial: " + obj.getFiducial());
      objects.remove(obj);
    }
  }

  for (int i = 0; i < objects.size(); i++) {
    Object obj = objects.get(i);
    obj.render();
  }

  if (objects.size() >= minimumMarkers) {
    if (equity != WAITING) {
      if (libertyCounter == objects.size()) {
        liberty = WAITING;
      } else if (brotherhoodCounter == objects.size()) {
        brotherhood = WAITING;
      }
    }
  }
}


void softResetPrinciples() {
  if (liberty == WAITING) {
    liberty = UNSOLVED;
  }  

  if (brotherhood == WAITING) {
    brotherhood = UNSOLVED;
  }  

  if (equity == WAITING) {
    equity = UNSOLVED;
  }
}

void hardResetPrincilples() {
  liberty = equity = brotherhood = UNSOLVED;
  writeSerial("reset");
}

// Serial

void readSerial() {
  while (serialPort != null && serialPort.available() > 0) {
    serialString = serialPort.readStringUntil(LF);

    if (serialString != null) {
      println(Serial.list()[0] + ": " + serialString);

      if (serialString.equals("Ready\r\n")) {
        serialReady = true;
      }
    }
  }
}

void writeSerial(String command) {
  // Possible commands: "set [leb] [usr]", "reset"

  if (serialReady) {
    serialPort.write(command + "\r");
  }
}

// Object functions

Object getObject(int fiducial) {
  for (int i = 0; i < objects.size(); i++) {
    Object obj = objects.get(i);
    if (obj.getFiducial() == fiducial) {
      return obj;
    }
  }

  return null;
}

Object createObject(int fiducial, float x, float y) {
  Object obj = new Object(fiducial, x, y);
  println("New object created. Fiducial: " + fiducial);

  objects.add(obj);
  return obj;
}

public void TUIOSet(String set, int id, int fiducial, float x, float y, float a, float vX, float vY, float vR, float mA, float rA) {
  // This will be called when a meessage with pattern "/tuio/2Dobj" and typetag "siiffffffff" is received

  float xMapped = map(x, 0, 1, 0, cameraDimensions.x);
  float yMapped = map(y, 0, 1, 0, cameraDimensions.y);

  Object obj = getObject(fiducial);

  if (obj == null) {
    obj = createObject(fiducial, x, y);
  }

  obj.setTime(millis() + aliveTime);
  obj.setPosition(xMapped, yMapped);
}

// Events

void oscEvent(OscMessage m) {
}

void keyPressed() {
  switch(key) {
  case 'a':
    float mx = mouseX - cameraOrigin.x;
    float my = mouseY - cameraOrigin.y;
    objects.add(new Object(-1, mx, my, false));
    break;

  case 'r': 
    for (int i = 0; i < objects.size(); i++) {
      Object obj = objects.get(i);

      if (obj.getSelected()) {
        objects.remove(i);
      }
    }
  }
}

void mouseDragged() {
  float mx = mouseX - cameraOrigin.x;
  float my = mouseY - cameraOrigin.y;

  boolean noneSelected = true;
  for (int i = 0; i < objects.size(); i++) {
    Object obj = objects.get(i);

    if (obj.getSelected()) {
      noneSelected = false;
    }
  }

  if (noneSelected) {
    for (int i = 0; i < objects.size(); i++) {
      Object obj = objects.get(i);
      obj.setSelected(false);
      if (dist(mx, my, obj.getPosition().x, obj.getPosition().y) < ELLIPSE_SIZE / 2) {
        obj.setSelected(true);
      }
      if (obj.getSelected()) {
        obj.setPosition(mx, my);
      }
    }
  } else {
    for (int i = 0; i < objects.size(); i++) {
      Object obj = objects.get(i);
      if (obj.getSelected()) {
        if (dist(mx, my, obj.getPosition().x, obj.getPosition().y) < ELLIPSE_SIZE / 2) {
          obj.setPosition(mx, my);
        } else {
          obj.setSelected(false);
        }
      }
    }
  }
}

void mouseClicked() {
  float mx = mouseX - cameraOrigin.x;
  float my = mouseY - cameraOrigin.y;

  for (int i = 0; i < objects.size(); i++) {
    Object obj = objects.get(i);
    obj.setSelected(false);
    if (dist(mx, my, obj.getPosition().x, obj.getPosition().y) < ELLIPSE_SIZE / 2) {
      obj.setSelected(true);
    }
  }
}
