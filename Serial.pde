static final String findPort() {
  String[] ports = Serial.list();
  //printArray(ports);
  //println(ports.length);

  for (String p : ports) {
    for (int i = 0; i <= 5; ++i) {
      if (p.equals("COM" + i) || p.equals("/dev/ttyACM" + i)) {
        println("Using port " + p);
        return p;
      }
    }
  }

  println("No serial port available");
  return null;
}
