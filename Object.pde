class Object {
  private PVector position;
  private int time;
  private boolean aging;
  private int fiducial;
  private color c;
  private boolean selected;

  Object() {
    this(-1, width / 2, height / 2, true);
    println("Created test object");
  }

  Object(int fiducial, float x, float y) {
    this(fiducial, width / 2, height / 2, true);
  }

  Object(int fiducial, float x, float y, boolean aging) {
    this.fiducial = fiducial;
    this.aging = aging;
    position = new PVector(x, y);
    c = color(random(0, 359), 100, 100);
  }

  void render() {
    pushStyle();
    if (selected) {
      fill(0, 0, 100);
    } else {
      fill(c);
    }
    noStroke();
    ellipse(position.x, position.y, ELLIPSE_SIZE, ELLIPSE_SIZE);
    popStyle();
  }

  void drawHelperLine(PVector target) {
    line(position.x, position.y, target.x, target.y);

    if (getSelected()) {
      float txt = degrees(atan2(target.y - position.y, target.x - position.x));

      PVector middle = PVector.lerp(position, target, 0.5);
      pushStyle();

      textSize(16);
      fill(GRAY_COLOR);
      text(txt, middle.x, middle.y);
      popStyle();
    }
  }

  void setTime(int time) {
    this.time = time;
  }

  void setPosition(float x, float y) {
    position.set(x, y);
  }

  void setPosition(PVector v) {
    position.set(v);
  }

  void setFiducial(int fiducial) {
    this.fiducial = fiducial;
  }

  void setSelected(boolean selected) {
    this.selected = selected;
  }

  int getTime() {
    return time;
  }

  int getFiducial() {
    return fiducial;
  }

  PVector getPosition() {
    return position;
  }

  boolean getAging() {
    return aging;
  }

  boolean getSelected() {
    return selected;
  }
}
