# Table 

Trabajo práctico 5 para Lenguaje Multimedial III. Año 2018.

## ¿Cómo ejecutar?

En primer lugar, se debe clonar el repositorio:

```
git clone https://gitlab.com/ianmethyst/tp5-table/
```

En el caso de que se desee ejecutar reacTIVision, se puede clonar el repositorio con su submódulo:

```
git clone --recurse-submodules https://github.com/ianmethyst/tp5-table
```

Además, para correr el sketch, es necesario instalar las siguientes bibliotecas en Processing:

- [Minim](http://code.compartmental.net/tools/minim/)
- [oscP5](http://www.sojamo.de/libraries/oscP5/)


## ¿Cómo usar?

El sketch funciona en conjunto con [reacTIVision](https://github.com/mkalten/reacTIVision), que es el proveedor de la información posicional de los objetos que se comparan para saber cuando se completó un puzzle. Aprovechando que OSC ya es una dependencia del proyecto (es el protocolo que utiliza reacTIVision), también es usado para comunicar este componente con el resto de las partes de la obra.

El código de Arduino se encuentra en la carpeta `table_arduino`, y fue probado en un Arduino Uno, pero debido a su simpleza (todo lo que hace es controlar 3 LEDs) debería poder correr en cualquier placa Arduino.

Debajo se encuentra un diagrama para realizar una conexión en una breadboard:

![./docs/fritzing_bb.png](Diagrama para realizar una conexión con un un Arduino Uno y una breadboard.)

## Debugging

Pulsando la tecla `a` se puede agregar un objeto. Al hacer click sobre un objeto, éste es seleccionado, y en el caso de que se arrastre el mouse al estar clickeando, se puede mover dicho objeto. Si hay un objeto seleccionado y se pulsa `r`, éste será eliminado.

El sketch en el directorio `table_tester` escucha por OSC, y se puede utilizar para probar que la parte de red del componente esté funcionado.

## TODO

Se debe pulir/modularizar el código que se ejecuta durante el loop `draw`. Idealmente, la aplicación debería poder correr en modo headless (sin dibujar nada), porque todos los elementos visuales son utilizados únicamente para el debugging, pero de momento, esto no es posible.
